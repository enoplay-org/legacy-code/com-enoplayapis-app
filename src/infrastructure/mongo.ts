import * as mongoose from 'mongoose'

import { Query, Change } from '../interfaces/repository/repo'

class MongoOptions {
  serverName: string
  databaseName: string
  constructor (serverName: string, databaseName: string) {
    this.serverName = serverName
    this.databaseName = databaseName
  }
}

class MongoHandler {
  db: mongoose.Connection
  options: MongoOptions
  constructor (options: MongoOptions) {
    this.options = options
  }

  newSession () {
    let session: mongoose.Connection = mongoose.createConnection(this.options.serverName)
    this.db = session.useDb(this.options.databaseName)
  }

  findOne (name: string, query: Query): Promise<any> {
    return this.db.collection(name).findOne(query)
  }

  insert (name: string, obj: any): Promise<any> {
    return this.db.collection(name).insert(obj)
  }

  update (name: string, query: Query, change: Change): Promise<any> {
    return this.db.collection(name).updateOne(query, change)
    .then(() => {
      return this.findOne(name, query)
    })
  }

  removeOne (name: string, query: Query): Promise<boolean> {
    return this.db.collection(name).deleteOne(query)
    .then(response => {
      return Promise.resolve(response ? true : false)
    })
  }

  exists (name: string, query: Query): Promise<boolean> {
    return this.db.collection(name).findOne(query)
    .then(response => {
      return Promise.resolve(response ? true : false)
    })
  }
}

export {
  MongoOptions,
  MongoHandler
}
