import axios from 'axios'

class EnoplayStaticOptions {
  apiBase: string
  constructor (apiBase: string) {
    this.apiBase = apiBase
  }
}

class EnoplayStaticHandler {
  options: EnoplayStaticOptions
  constructor (options: EnoplayStaticOptions) {
    this.options = options
  }

  delete (sessionToken: string, uid: string): Promise<boolean> {
    return axios({
      method: 'delete',
      url: `${this.options.apiBase}/images/${uid}`,
      headers: {
        'Authorization': `Bearer ${sessionToken}`,
        'Content-Type': 'application/json'
      }
    })
    .then(() => {
      return Promise.resolve(true)
    })
    .catch(err => {
      return Promise.reject(`Error deleting image from enoplayapis_static: ${err}`)
    })
  }
}

export {
  EnoplayStaticOptions,
  EnoplayStaticHandler
}
