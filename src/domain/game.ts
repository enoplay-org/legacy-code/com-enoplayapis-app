interface GameRepository {
  getByUID (uid: string): Promise<Game>
  getByGID (gid: string): Promise<Game>

  updateApp (game: Game): Promise<Game>

  delete (sessionToken: string, game: Game): Promise<boolean>
  deleteThumbnail (sessionToken: string, game: Game): Promise<Game>
  deleteImages (sessionToken: string, game: Game): Promise<Game>
}

class Game {
  uid: string
  gid: string
  title: string
  description: string
  media: {
    thumbnail: {
      uid: string
      source: string
    }
    images: { [key: string]: {
      uid: string
      source: string
    }}
  }
  publisher: {
    uid: string
    username: string
  }
  app: GameApp
}

class GameApp {
  uid: string
  aid: string
  webURL: string
}

export {
  GameRepository,
  Game,
  GameApp
}
