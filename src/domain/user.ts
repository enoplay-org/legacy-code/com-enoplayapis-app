interface UserRepository {
  getByUID (uid: string): Promise<User>

  unpublishGame (userUID: string, gameUID: string): Promise<boolean>

  publishApp (userUID: string, appPublishItem: UserAppPublishHistoryItem): Promise<boolean>
  unpublishApp (userUID: string, appUID: string): Promise<boolean>
}

class User {
  uid: string
  username: string
  alias: string
  media: UserMedia
}

interface UserMedia {
  icon: { source: string }
}

interface UserGamePublishHistoryItem {
  gameUID: string
  dateAdded: Date
}

interface UserAppPublishHistoryItem {
  appUID: string
  dateAdded: Date
}

export {
  UserRepository,
  User,
  UserGamePublishHistoryItem,
  UserAppPublishHistoryItem
}
