import { SessionTokenRepository } from '../domain/sessionToken'

class SessionTokenInteractor {
  sessionTokenRepository: SessionTokenRepository

  setSessionTokenRepository (sessionTokenRepository: SessionTokenRepository) {
    this.sessionTokenRepository = sessionTokenRepository
  }

  verifyAccess (token: string): Promise<string> {
    return new Promise((resolve, reject) => {
      return this.sessionTokenRepository.verifyAccess(token)
        .then(claim => {
          resolve(claim.userUID)
        })
        .catch(err => {
          reject(`Failed to verify token: ${err}`)
        })
    })
  }
}

export {
  SessionTokenInteractor
}
