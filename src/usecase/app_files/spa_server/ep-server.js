var path = require('path')
var express = require('express')
var ep_middleware = require('./ep-middleware')

var server = express()

server = ep_middleware.setup(server)
server.use(express.static(__dirname))

server.get('/*', function (req, res) {
  res.sendFile(path.resolve(__dirname, './index.html'))
})

var port = process.env.PORT || 3005
server.listen(port, function () {
  console.log('server listening on port ' + port)
})
