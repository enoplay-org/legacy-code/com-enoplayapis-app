var path = require('path')
var cors = require('cors')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var axios = require('axios')

const ENOPLAY_API_BASE = '{{ apiHostURL }}'
const MAX_COOKIE_AGE = 1000 * 60 * 60 * 1 // 1 hour
const COOKIE_SECRET = 'something-super-secret'
const GAME_ID = '{{ gid }}'

const EP_USER_UID = 'EP_USER_UID'
const EP_USERNAME = 'EP_USERNAME'
const EP_USER_ALIAS = 'EP_USER_ALIAS'
const EP_USER_IS_ANON = 'EP_USER_IS_ANON'

function verifyToken (req, res) {
  var token = req.body['token']

  if (!token) {
    res.json({
      message: `Token is required.`,
      success: false
    })
    return
  }
  // Verify token
  axios.post(`${ENOPLAY_API_BASE}/plays/token_verification`, {
    token: token,
    gid: GAME_ID
  })
  .then(function (response) {
    var data = response.data
    if (!data.success) {
      return Promise.reject(data.message)
    }
    // Set cookies
    res.cookie(EP_USER_UID, data.user.uid, { httpOnly: true, signed: true, maxAge: MAX_COOKIE_AGE })
    res.cookie(EP_USERNAME, data.user.username, { httpOnly: true, signed: true, maxAge: MAX_COOKIE_AGE  })
    res.cookie(EP_USER_ALIAS, data.user.alias, { httpOnly: true, signed: true, maxAge: MAX_COOKIE_AGE  })
    res.cookie(EP_USER_IS_ANON, data.isAnon, { httpOnly: true, signed: true, maxAge: MAX_COOKIE_AGE  })

    res.json({
      message: 'Successfully verified token',
      success: true
    })
  })
  .catch(function (err) {
    var responseError = err
    if (err.response && err.response.data && err.response.data.message) {
      responseError = err.response.data.message
    }
    res.json({
      message: `Error verifying token: ${responseError}`,
      success: false
    })
  })
}

function ensureAuth (req, res, next) {
  var isAuthorized = req.signedCookies[EP_USER_UID] && req.signedCookies[EP_USERNAME] && req.signedCookies[EP_USER_ALIAS]
  if (!isAuthorized) {
    res.sendFile(path.resolve(__dirname, './auth-index.html'))
    return
  }
  next()
}

function setupMiddleware (router) {
  router.use(cors())
  router.use(bodyParser.json())
  router.use(cookieParser(COOKIE_SECRET))
  router.post('/enoplay/plays/token_verification', verifyToken)
  router.use(ensureAuth)
  return router
}

module.exports = {
  setup: setupMiddleware
}
