import { UserRepository, User } from '../domain/user'

class UserInteractor {
  userRepository: UserRepository

  setUserRepository (userRepository: UserRepository) {
    this.userRepository = userRepository
  }

  getByUID (uid: string): Promise<User> {
    return this.userRepository.getByUID(uid)
  }
}

export {
  UserInteractor
}
