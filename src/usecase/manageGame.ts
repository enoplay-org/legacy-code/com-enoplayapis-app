import { GameRepository, Game } from '../domain/game'
import { AppRepository } from '../domain/app'
import { UserRepository } from '../domain/user'

class GameInteractor {
  gameRepository: GameRepository
  appRepository: AppRepository
  userRepository: UserRepository

  setGameRepository (gameRepository: GameRepository) {
    this.gameRepository = gameRepository
  }

  setAppRepository (appRepository: AppRepository) {
    this.appRepository = appRepository
  }

  setUserRepository (userRepository: UserRepository) {
    this.userRepository = userRepository
  }

  getByGID (gid: string): Promise<Game> {
    return this.gameRepository.getByGID(gid)
  }

  delete (sessionToken: string, game: Game): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // Delete App
      this.appRepository.getByUID(game.app.uid)
      .then(appResponse => {
        this.appRepository.deleteWithForce(appResponse)
        .catch(() => { /* Do nothing for now */ })
      })
      // Remove App from User's publish history
      .then(() => {
        return this.userRepository.unpublishApp(game.publisher.uid, game.app.uid)
      })
      .catch(() => { /* Do nothing for now */ })

      // Delete Game
      return this.gameRepository.delete(sessionToken, game)
      // Remove Game from publisher's history
      .then(() => {
        return this.userRepository.unpublishGame(game.publisher.uid, game.uid)
      })
      .then(() => {
        resolve(true)
      })
      .catch(err => {
        reject(`Error deleting game: ${err}`)
      })
    })
  }
}

export {
  GameInteractor
}
