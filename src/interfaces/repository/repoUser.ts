import { DbRepo, DbHandler, Query, Change } from './repo'
import { User, UserAppPublishHistoryItem } from '../../domain/user'

const USER_COLLECTION = 'users'
const USER_GAME_PUBLISH_COLLECTION = 'users_game_publish'
const USER_APP_PUBLISH_COLLECTION = 'users_app_publish'

class DbUserRepo extends DbRepo {
  constructor (dbHandlers: { [key: string]: DbHandler }) {
    super(dbHandlers, dbHandlers[DbUserRepo.getClassName()])
  }

  static getClassName (): string { return 'DbUserRepo' }

  getByUID (uid: string): Promise<User> {
    return new Promise((resolve, reject) => {
      return this.dbHandler.findOne(USER_COLLECTION, { uid: uid })
      .then(response => {
        let user: User = response
        if (!user) {
          return Promise.reject('Error finding user from repo')
        }
        return Promise.resolve(user)
      })
      .then(userResponse => {
        resolve(userResponse)
      })
      .catch(err => {
        reject(err)
      })
    })
  }

  // unpublishGame removes a Game from a User's publish history
  unpublishGame (userUID: string, gameUID: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let query: Query = {}
      query[`history.${gameUID}`] = 1

      let change: Change = {
        $unset: query
      }

      return this.dbHandler.update(USER_GAME_PUBLISH_COLLECTION, { 'userUID': userUID }, change)
      .then(resolve)
      .catch(reject)
    })
  }

  // publishApp adds an App to the User's App publish history
  publishApp (userUID: string, appPublishItem: UserAppPublishHistoryItem): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let query: Query = {}
      query[`history.${appPublishItem.appUID}`] = appPublishItem

      let change: Change = {
        $set: query
      }

      return this.dbHandler.update(USER_APP_PUBLISH_COLLECTION, { 'userUID': userUID }, change)
      .then(resolve)
      .catch(reject)
    })
  }

  // unpublishApp removes an App from a User's App publish history
  unpublishApp (userUID: string, appUID: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let query: Query = {}
      query[`history.${appUID}`] = 1

      let change: Change = {
        $unset: query
      }

      return this.dbHandler.update(USER_APP_PUBLISH_COLLECTION, { 'userUID': userUID }, change)
      .then(resolve)
      .catch(reject)
    })
  }
}

export {
  DbUserRepo
}
