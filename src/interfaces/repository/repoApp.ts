import * as bson from 'bson'

import {
  DbRepo,
  DbHandler,
  Query,
  Change,
  GitRepoHandler,
  HostHandler
} from './repo'

import {
  App,
  AppStatus,
  AppVersionHistory,
  AppVersionHistoryItem,
  APP_STATUS_PENDING,
  APP_STATUS_DELETED
} from '../../domain/app'

const APP_COLLECTION = 'apps'
const APP_VERSION_COLLECTION = 'apps_version'
const APP_DELETED_COLLECTION = 'apps_deleted'

const APP_VERSION_TITLE_PREFIX = 'Version'

class DbAppRepo extends DbRepo {
  gitRepoHandler: GitRepoHandler
  hostHandler: HostHandler
  constructor (dbHandlers: { [key: string]: DbHandler }, gitRepoHandler: GitRepoHandler, hostHandler: HostHandler) {
    super(dbHandlers, dbHandlers[DbAppRepo.getClassName()])
    this.gitRepoHandler = gitRepoHandler
    this.hostHandler = hostHandler
  }

  static getClassName (): string { return 'DbAppRepo' }

  getByUID (uid: string): Promise<App> {
    return new Promise((resolve, reject) => {
      return this.dbHandler.findOne(APP_COLLECTION, { 'uid': uid })
      .then(response => {
        let app: App = response
        if (!app) {
          return Promise.reject('Error finding App from repo')
        }
        return Promise.resolve(app)
      })
      .then(appResponse => {
        resolve(appResponse)
      })
      .catch(err => {
        reject(`Error retrieving app by uid from repo: ${err}`)
      })
    })
  }

  getByAID (aid: string): Promise<App> {
    return new Promise((resolve, reject) => {
      return this.dbHandler.findOne(APP_COLLECTION, { 'aid': aid })
      .then(response => {
        let app: App = response
        if (!app) {
          return Promise.reject('Error finding App from repo')
        }
        return Promise.resolve(app)
      })
      .then(appResponse => {
        resolve(appResponse)
      })
      .catch(err => {
        reject(`Error retrieving app by aid from repo: ${err}`)
      })
    })
  }

  // getStatus retrieves the host app status of the activeVersions of the App
  getStatus (app: App): Promise<AppStatus> {
    return new Promise((resolve, reject) => {
      let devVersionUID = app.versionsActive.dev
      let prodVersionUID = app.versionsActive.prod

      let appStatus: AppStatus = new AppStatus()

      let promises: Promise<any>[] = []

      // Retrieve dev app status
      if (devVersionUID) {
        promises.push(
          // Get dev App version
          this.getVersion(app.uid, devVersionUID)
          .then(devVersionResponse => {
            let appID = devVersionResponse.hostDetails.appID

            // Get build status
            return this.hostHandler.getBuildStatus(appID)
            .then(statusResponse => {
              appStatus.versionsActive.dev.hostDetailsStatus = statusResponse
              // Get runtime status
              return this.hostHandler.getRuntimeStatus(appID)
              .then(runtimeStatusResponse => {
                appStatus.versionsActive.dev.hostDetailsStatus.runtimeLogDetails = runtimeStatusResponse
              })
            })
          })
        )
      }

      // Retrieve prod app status
      if (prodVersionUID) {
        promises.push(
          // Get prod App version
          this.getVersion(app.uid, prodVersionUID)
          .then(prodVersionResponse => {
            let appID = prodVersionResponse.hostDetails.appID

            // Get build status
            return this.hostHandler.getBuildStatus(appID)
            .then(statusResponse => {
              appStatus.versionsActive.prod.hostDetailsStatus = statusResponse
              // Get runtime status
              return this.hostHandler.getRuntimeStatus(appID)
              .then(runtimeStatusResponse => {
                appStatus.versionsActive.prod.hostDetailsStatus.runtimeLogDetails = runtimeStatusResponse
              })
            })
          })
        )
      }

      return Promise.all(promises)
      // Respond
      .then(() => {
        resolve(appStatus)
      })
      .catch(err => {
        reject(`Error retrieving App status: ${err}`)
      })
    })
  }

  create (app: App, language: string, filePath: string): Promise<App> {
    return new Promise((resolve, reject) => {
      app.uid = (new bson.ObjectID()).toHexString()
      app.status = APP_STATUS_PENDING
      app.dateCreated = new Date()

      // Setup first version of the App
      let appVersion: AppVersionHistoryItem = new AppVersionHistoryItem()
      appVersion.uid = `0-${generateVersionUIDHash()}`
      appVersion.title = `${APP_VERSION_TITLE_PREFIX} ${appVersion.uid}`
      appVersion.language = language

      // Setup App version history
      return this.setupAppVersionHistory(app.uid)
      // Create git repo
      .then(() => {
        return this.gitRepoHandler.create(app.aid, appVersion.uid, filePath)
        .then(repoResponse => {
          appVersion.gitRepoDetails = repoResponse
          return repoResponse
        })
      })
      // Create host app
      .then(repoResponse => {
        return this.hostHandler.create(repoResponse.tarball)
        .then(hostResponse => {
          appVersion.hostDetails = hostResponse
        })
      })
      // Remove tarball to avoid exposing secrets
      .then(() => {
        appVersion.gitRepoDetails.tarball = ''
      })
      // Insert to db
      .then(() => {
        app.versionsActive.prod = appVersion.uid
        return this.dbHandler.insert(APP_COLLECTION, app)
      })
      // Update version history
      .then(() => {
        return this.addVersion(app.uid, appVersion)
      })
      // Return app
      .then(() => {
        resolve(app)
      })
      .catch(err => {
        // Delete host app
        this.hostHandler.delete(appVersion.hostDetails.appID)
        .catch(() => { /* Do nothing for now */ })

        // Delete git repo
        this.gitRepoHandler.delete(appVersion.gitRepoDetails.repoID)
        .catch(() => { /* Do nothing for now */ })
        reject(`Error create app from repo: ${err}`)
      })
    })
  }

  updateVersionsActive (app: App): Promise<App> {
    return new Promise((resolve, reject) => {
      let query: Query = { versionsActive: app.versionsActive }
      let change: Change = {
        $set: query
      }
      return this.dbHandler.update(APP_COLLECTION, { uid: app.uid }, change)
      .then(appResponse => {
        let updatedApp: App = appResponse
        if (!updatedApp) {
          return Promise.reject('Error finding app from repo')
        }
        return Promise.resolve(updatedApp)
      })
      .then(appResponse => {
        resolve(appResponse)
      })
      .catch(err => {
        reject(`Error updating game App versions active in repo: ${err}`)
      })
    })
  }

  updateFile (app: App, language: string, filePath: string, hostAppID?: string): Promise<App> {
    return new Promise((resolve, reject) => {

      let appVersion: AppVersionHistoryItem

      // Setup App version
      return this.getVersionHistory(app.uid)
      .then(historyResponse => {
        appVersion = new AppVersionHistoryItem()
        appVersion.uid = `${Object.keys(historyResponse.versions).length}-${generateVersionUIDHash()}`
        appVersion.title = `${APP_VERSION_TITLE_PREFIX} ${appVersion.uid}`
        appVersion.language = language
      })
      // Update git repo
      .then(() => {
        return this.gitRepoHandler.update(app.aid, appVersion.uid, filePath)
        .then(repoResponse => {
          appVersion.gitRepoDetails = repoResponse
          return repoResponse
        })
      })
      // Create host app
      .then(repoResponse => {
        if (hostAppID) {
          return this.hostHandler.update(hostAppID, repoResponse.tarball)
        }
        return this.hostHandler.create(repoResponse.tarball)
      })
      .then(hostResponse => {
        appVersion.hostDetails = hostResponse
      })
      // Remove tarball to avoid exposing secrets
      .then(() => {
        appVersion.gitRepoDetails.tarball = ''
      })
      // Update App versions active
      .then(() => {
        app.versionsActive.dev = appVersion.uid
        return this.updateVersionsActive(app)
      })
      // Update version history
      .then(() => {
        return this.addVersion(app.uid, appVersion)
      })
      // Return app
      .then(() => {
        resolve(app)
      })
      .catch(err => {
        // Delete new host app
        if (!hostAppID) {
          this.hostHandler.delete(appVersion.hostDetails.appID)
          .catch(() => { /* Do nothing for now */ })
        }
        reject(`Error updating app file with ${hostAppID ? 'old' : 'new'} host from repo: ${err}`)
      })
    })
  }

  deleteWithForce (app: App): Promise<boolean> {
    return new Promise((resolve, reject) => {

      let hostAppIDs: { [key: string]: boolean } = {}
      let gitRepoIDs: { [key: string]: boolean } = {}

      app.status = APP_STATUS_DELETED

      this.getVersionHistory(app.uid)
      // Delete all host apps and git repos
      .then(historyResponse => {
        if (!historyResponse || !historyResponse.versions) {
          return
        }

        for (let versionUID in historyResponse.versions) {
          if (!historyResponse.versions.hasOwnProperty(versionUID)) {
            continue
          }

          let hostAppID = historyResponse.versions[versionUID].hostDetails.appID
          let gitRepoID = historyResponse.versions[versionUID].gitRepoDetails.repoID

          // Delete host app
          if (!hostAppIDs[hostAppID]) {
            hostAppIDs[hostAppID] = true
            this.hostHandler.delete(hostAppID)
            .catch(() => { /* Do nothing for now */ })
          }

          // Delete git repo
          if (!gitRepoIDs[gitRepoID]) {
            gitRepoIDs[gitRepoID] = true
            this.gitRepoHandler.delete(gitRepoID)
            .catch(() => { /* Do nothing for now */ })
          }
        }
      })
      // Delete App
      .then(() => {
        // Add App to deleted collection
        return this.dbHandler.insert(APP_DELETED_COLLECTION, app)
        // Remove App from collection
        .then(() => {
          return this.dbHandler.removeOne(APP_COLLECTION, { uid: app.uid })
        })
        // Remove version history
        .then(() => {
          return this.deleteVersionHistory(app.uid)
        })
        // Respond
        .then(isDeleted => {
          resolve(isDeleted)
        })
        .catch(err => {
          reject(`Error deleting App from repo: ${err}`)
        })
      })
      .catch(() => { /* Do nothing for now */ })
    })
  }

  existsByAID (aid: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      return this.dbHandler.exists(APP_COLLECTION, { 'aid': aid })
      .then(exists => {
        resolve(exists)
      })
      .catch(err => {
        reject(`Error determining the existence of app by aid from repo: ${err}`)
      })
    })
  }

  setupAppVersionHistory (appUID: string): Promise<AppVersionHistory> {
    let appVersionHistory: AppVersionHistory = new AppVersionHistory()
    appVersionHistory.appUID = appUID
    return this.dbHandler.insert(APP_VERSION_COLLECTION, appVersionHistory)
    .then(() => {
      return appVersionHistory
    })
    .catch(err => {
      return Promise.reject(`Error setting up app version history: ${err}`)
    })
  }

  getVersion (appUID: string, versionUID: string): Promise<AppVersionHistoryItem> {
    return new Promise((resolve, reject) => {
      return this.dbHandler.findOne(APP_VERSION_COLLECTION, { 'appUID': appUID })
      .then((appVersionHistoryResponse: AppVersionHistory) => {
        return appVersionHistoryResponse.versions[versionUID]
      })
      .then(appVersionHistoryItem => {
        if (!appVersionHistoryItem) {
          return Promise.reject(`Error finding app version ${versionUID}`)
        }
        resolve(appVersionHistoryItem)
      })
      .catch(err => {
        reject(`Error retrieving version ${versionUID} of app ${appUID}: ${err}`)
      })
    })
  }

  getVersionHistory (appUID: string): Promise<AppVersionHistory> {
    return this.dbHandler.findOne(APP_VERSION_COLLECTION, { 'appUID': appUID })
    .catch(err => {
      return Promise.reject(`Error retrieving version history: ${err}`)
    })
  }

  addVersion (appUID: string, version: AppVersionHistoryItem): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let query: Query = {}
      query[`versions.${version.uid}`] = version

      let change: Change = {
        $set: query
      }
      return this.dbHandler.update(APP_VERSION_COLLECTION, { 'appUID': appUID }, change)
      .then(resolve)
      .catch(err => {
        reject(`Error adding version ${version.uid} to to app ${appUID}: ${err}`)
      })
    })
  }

  deleteVersionHistory (appUID: string): Promise<boolean> {
    return this.dbHandler.removeOne(APP_VERSION_COLLECTION, { 'appUID': appUID })
    .catch(err => {
      return Promise.reject(`Error deleting app version history: ${err}`)
    })
  }
}

const versionUIDHashLength = 4
const versionCharacters = '01234556789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-'

function generateVersionUIDHash (): string {
  let hash: string = ''
  for (let i = 0; i < versionUIDHashLength; i++) {
    let location: number = Math.floor(Math.random() * versionCharacters.length)
    hash += versionCharacters.charAt(location)
  }
  return hash
}

export {
  DbAppRepo
}
