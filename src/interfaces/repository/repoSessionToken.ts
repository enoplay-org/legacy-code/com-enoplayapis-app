import * as jwt from 'jsonwebtoken'

import { DbRepo, DbHandler } from './repo'
import { TokenAuthorityOptions } from './tokenAuthority'

import { SessionTokenClaim, SESSION_TOKEN_TYPE_ACCESS } from '../../domain/sessionToken'

class DbSessionTokenRepo extends DbRepo {
  taOptions: TokenAuthorityOptions
  constructor (dbHandlers: { [key: string]: DbHandler }, taOptions: TokenAuthorityOptions) {
    super(dbHandlers, dbHandlers[DbSessionTokenRepo.getClassName()])
    this.taOptions = taOptions
  }

  static getClassName (): string { return 'DbSessionTokenRepo' }

  // verifyAccess checks if an access token is valid
  verifyAccess (tokenString: string): Promise<SessionTokenClaim> {
    return new Promise((resolve, reject) => {
      let claim: SessionTokenClaim

      jwt.verify(tokenString, this.taOptions.publicSigningKey, (err, data) => {
        if (err) {
          reject(`Unexpected signing method: ${err}`)
          return
        }
        claim = <SessionTokenClaim>data

        // Ensure token is access token
        if (claim.type !== SESSION_TOKEN_TYPE_ACCESS) {
          reject(`Invalid access token`)
          return
        }

        // Ensure token has not expired
        let dateExpired = new Date(0)
        dateExpired.setSeconds(Number(claim.dateExpired.toString()))
        let dateNow = new Date()
        if (dateNow.getTime() - dateExpired.getTime() > 0) {
          reject(`Access token has expired`)
          return
        }

        // Respond with claim
        resolve(claim)
      })
    })
  }
}

export {
  DbSessionTokenRepo
}
