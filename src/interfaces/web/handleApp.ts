import { Request, Response } from './express.d'

import { App, AppStatus, AppVersionHistoryItem, User, Game } from '../../domain'
import { AppInteractor, UserInteractor, GameInteractor } from './interactors'
import { Utility, ZIP_FILE_FIELDNAME } from './util'

interface GetAppResponseV0 {
  app: App
  message: string
  success: boolean
}

interface GetAppStatusResponseV0 {
  status: AppStatus
  message: string
  success: boolean
}

interface GetAppVersionsResponseV0 {
  versions: { [key: string]: AppVersionHistoryItem }
  message: string
  success: boolean
}

interface CreateAppRequestV0 {
  gid: string
  language: string
}

interface CreateAppResponseV0 {
  app: App
  message: string
  success: boolean
}

interface UpdateAppVersionLiveRequestV0 {
  versionUID: string
}

interface UpdateAppVersionLiveResponseV0 {
  app: App
  message: string
  success: boolean
}

interface UpdateAppFileRequestV0 {
  language: string
}

interface UpdateAppFileResponseV0 {
  app: App
  message: string
  success: boolean
}

interface DeleteAppRequestV0 {
  force: boolean
}

interface DeleteAppResponseV0 {
  message: string
  success: boolean
}

class AppHandler {
  util: Utility
  appInteractor: AppInteractor
  userInteractor: UserInteractor
  gameInteractor: GameInteractor

  // getStatus retrieves the setup and runtime status of an App's host application
  getStatus (req: Request, res: Response) {
    if (!req.userUID) {
      this.util.renderError(req, res, 401, 'Invalid authentication token.')
      return
    }

    // Ensure App exists
    this.appInteractor.getByAID(req.params['aid'])
    .then(appResponse => {
      if (appResponse.publisher.uid !== req.userUID) {
        return Promise.reject('Error: Unauthorized access to App')
      }
      return Promise.resolve(appResponse)
    })
    // Retrieve response
    .then(appResponse => {
      return this.appInteractor.getStatus(appResponse)
    })
    // Respond
    .then(appStatusResponse => {
      let response: GetAppStatusResponseV0 = {
        status: appStatusResponse,
        message: 'Successfully retrieved app status!',
        success: true
      }
      this.util.render(req, res, 200, response)
    })
    .catch(err => {
      this.util.renderError(req, res, 401, `Error retrieving app status: ${err}`)
    })
  }

  getVersions (req: Request, res: Response) {
    if (!req.userUID) {
      this.util.renderError(req, res, 401, 'Invalid authentication token.')
      return
    }

    // Ensure App exists
    this.appInteractor.getByAID(req.params['aid'])
    .then(appResponse => {
      if (appResponse.publisher.uid !== req.userUID) {
        return Promise.reject('Error: Unauthorized access to App')
      }
      return Promise.resolve(appResponse)
    })
    // Retrieve response
    .then(appResponse => {
      return this.appInteractor.getVersions(appResponse)
    })
    // Respond
    .then(appVersionsResponse => {
      let response: GetAppVersionsResponseV0 = {
        versions: appVersionsResponse.versions,
        message: 'Successfully retrieved app versions!',
        success: true
      }
      this.util.render(req, res, 200, response)
    })
    .catch(err => {
      this.util.renderError(req, res, 401, `Error retrieving app versions: ${err}`)
    })
  }

  // retrieve returns an App by its aid
  retrieve (req: Request, res: Response) {
    if (!req.userUID) {
      this.util.renderError(req, res, 401, 'Invalid authentication token.')
      return
    }

    this.appInteractor.getByAID(req.params['aid'])
    .then(appResponse => {
      if (appResponse.publisher.uid !== req.userUID) {
        return Promise.reject('Error: Unauthorized access to App')
      }
      return Promise.resolve(appResponse)
    })
    .then(appResponse => {
      let response: GetAppResponseV0 = {
        app: appResponse,
        message: 'Successfully retrieved app!',
        success: true
      }
      this.util.render(req, res, 200, response)
    })
    .catch(err => {
      this.util.renderError(req, res, 401, `Error retrieving app: ${err}`)
    })
  }

  // create generates an App for a given Game
  create (req: Request, res: Response) {
    if (!req.userUID) {
      this.util.renderError(req, res, 401, 'Invalid authentication token.')
      return
    }

    // Validate input
    if (!req.body.gid || !req.body.language || !(req.file && req.file.fieldname === ZIP_FILE_FIELDNAME)) {
      this.util.renderError(req, res, 401, 'Invalid request body.')
      return
    }

    // Setup paramaters
    let body: CreateAppRequestV0 = { gid: req.body.gid, language: req.body.language }
    let fileName: string = req.file.filename.slice(0, -4)
    let originalFileName: string = req.file.originalname.slice(0, -4)

    let appGame: Game
    let appGamePublisher: User

    // Ensure Game exists
    this.gameInteractor.getByGID(body.gid)
    .then(gameResponse => {
      if (gameResponse.publisher.uid !== req.userUID) {
        return Promise.reject('Unauthorized access to game')
      }
      if (gameResponse.app && (gameResponse.app.uid !== '' || gameResponse.app.aid !== '')) {
        return Promise.reject('Error: Game already has app')
      }
      appGame = gameResponse
      return
    })
    // Ensure User exists
    .then(() => {
      return this.userInteractor.getByUID(req.userUID)
      .then(userResponse => {
        appGamePublisher = userResponse
      })
    })
    // Unzip the file contents
    .then(() => {
      return this.util.unzipFile(fileName)
    })
    // Create App
    .then(() => {
      const filePath: string = `${this.util.FILE_PATH_UNZIPPED}/${fileName}/${originalFileName}`
      return this.appInteractor.create(appGame, appGamePublisher, body.language, filePath)
    })
    // Respond
    .then(appResponse => {
      let response: CreateAppResponseV0 = {
        app: appResponse,
        message: 'Successfully created app!',
        success: true
      }
      this.util.render(req, res, 200, response)
    })
    .catch(err => {
      // Delete Game
      this.gameInteractor.delete(req.sessionToken, appGame)
      this.util.renderError(req, res, 401, `Error creating app: ${err}`)
    })
  }

  // updateFile updates the App's website with a given file
  updateFile (req: Request, res: Response) {
    // Validate input
    if (!req.userUID) {
      this.util.renderError(req, res, 401, 'Invalid authentication token.')
      return
    }
    if (!req.body.language || !(req.file && req.file.fieldname === ZIP_FILE_FIELDNAME)) {
      this.util.renderError(req, res, 401, 'Invalid request body.')
      return
    }

    // Setup parameters
    let body: UpdateAppFileRequestV0 = { language: req.body.language }
    let fileName: string = req.file.filename.slice(0, -4)
    let originalFileName: string = req.file.originalname.slice(0, -4)

    let app: App
    let appPublisher: User

    // Ensure App exists
    this.appInteractor.getByAID(req.params['aid'])
    .then(appResponse => {
      if (appResponse.publisher.uid !== req.userUID) {
        return Promise.reject('Error: Unauthorized access to App')
      }
      app = appResponse
      return
    })
    // Ensure User Exists
    .then(() => {
      return this.userInteractor.getByUID(req.userUID)
      .then(userResponse => {
        appPublisher = userResponse
      })
    })
    // Unzip the file contents
    .then(() => {
      return this.util.unzipFile(fileName)
    })
    // Update App
    .then(() => {
      const filePath: string = `${this.util.FILE_PATH_UNZIPPED}/${fileName}/${originalFileName}`
      return this.appInteractor.updateFile(app, appPublisher, body.language, filePath)
    })
    // Respond
    .then(appResponse => {
      let response: UpdateAppFileResponseV0 = {
        app: appResponse,
        message: 'Successfully updated app!',
        success: true
      }
      this.util.render(req, res, 200, response)
    })
    .catch(err => {
      this.util.renderError(req, res, 401, `Error updating app: ${err}`)
    })
  }

  // updateVersionProd sets the production version of the App
  updateVersionProd (req: Request, res: Response) {
    // Validate input
    if (!req.userUID) {
      this.util.renderError(req, res, 401, 'Authentication required.')
      return
    }
    if (req.body.versionUID === '') {
      this.util.renderError(req, res, 401, 'Invalid request body.')
      return
    }

    // Setup parameters
    let body: UpdateAppVersionLiveRequestV0 = { versionUID: req.body.versionUID }
    let app: App

    // Ensure App exists
    this.appInteractor.getByAID(req.params['aid'])
    .then(appResponse => {
      if (appResponse.publisher.uid !== req.userUID) {
        return Promise.reject('Error: Unauthorized access to App')
      }
      app = appResponse
      return
    })
    // Update App
    .then(() => {
      return this.appInteractor.updateVersionProd(app, body.versionUID)
    })
    // Respond
    .then(appResponse => {
      let response: UpdateAppVersionLiveResponseV0 = {
        app: appResponse,
        message: 'Successfully updated app version',
        success: true
      }
      this.util.render(req, res, 200, response)
    })
    .catch(err => {
      this.util.renderError(req, res, 401, `Error updating app version: ${err}`)
    })
  }

  // delete schedules an App for permanent deletion or immediately removes it if force flag is received
  delete (req: Request, res: Response) {
    // Validate input
    if (!req.userUID) {
      this.util.renderError(req, res, 401, 'Invalid authentication token.')
      return
    }

    // Setup parameters
    let body: DeleteAppRequestV0 = { force: req.body.force || false }
    let app: App

    // Ensure App exists
    this.appInteractor.getByAID(req.params['aid'])
    .then(appResponse => {
      if (appResponse.publisher.uid !== req.userUID) {
        return Promise.reject('Error: Unauthorized access to App')
      }
      app = appResponse
      return
    })
    // Delete App
    .then(() => {
      if (body.force) {
        return this.appInteractor.deleteWithForce(app)
      }
    })
    // Respond
    .then(() => {
      let response: DeleteAppResponseV0 = {
        message: 'Successfully deleted app!',
        success: true
      }
      this.util.render(req, res, 200, response)
    })
    .catch(err => {
      this.util.renderError(req, res, 401, `Error deleting app: ${err}`)
    })
  }
}

export {
  AppHandler
}
