import { App, AppStatus, AppVersionHistory, User, Game } from '../../domain'

interface AppInteractor {
  getByUID (uid: string): Promise<App>
  getByAID (aid: string): Promise<App>
  getStatus (app: App): Promise<AppStatus>
  getVersions (app: App): Promise<AppVersionHistory>

  create (game: Game, publisher: User, language: string, filePath: string): Promise<App>
  updateVersionProd (app: App, versionUID: string): Promise<App>
  updateFile (app: App, publisher: User, language: string, filePath: string): Promise<App>

  deleteWithForce (app: App): Promise<boolean>
}

interface SessionTokenInteractor {
  verifyAccess (token: string): Promise<string>
}

interface UserInteractor {
  getByUID (uid: string): Promise<User>
}

interface GameInteractor {
  getByGID (gid: string): Promise<Game>
  delete (sessionToken: string, game: Game): Promise<boolean>
}

export {
  AppInteractor,
  SessionTokenInteractor,
  UserInteractor,
  GameInteractor
}
