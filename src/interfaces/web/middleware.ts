import * as path from 'path'
import * as multer from 'multer'
import * as fse from 'fs-extra'
import * as onFinished from 'on-finished'
import * as uuid from 'uuid'

import { Request, Response, NextFunction } from './express.d'

import { SessionTokenInteractor } from './interactors'
import { Utility, ZIP_FILE_FIELDNAME } from './util'

class Middleware {
  util: Utility
  sessionTokenInteractor: SessionTokenInteractor

  ensureAuthorization (req: Request, res: Response, next: NextFunction) {
    let authHeader: string = req.headers['authorization']
    if (!authHeader) {
      this.util.renderError(req, res, 401, 'Requires Authentication: Invalid Bearer Header.')
      return
    }
    let authHeaderArr: string[] = authHeader.split(' ')
    let authSchema: string = authHeaderArr[0]
    let bearerToken: string = authHeaderArr[1]

    if (authSchema !== 'Bearer') {
      this.util.renderError(req, res, 401, 'Requires Authentication: Invalid Bearer Header.')
      return
    }

    if (!bearerToken) {
      this.util.renderError(req, res, 401, 'Requires Authentication: Invalid Bearer Header.')
      return
    }

    this.sessionTokenInteractor.verifyAccess(bearerToken)
    .then((response: string) => {
      req.userUID = response
      req.sessionToken = bearerToken
      next()
    })
    .catch(err => {
      this.util.renderError(req, res, 401, 'Invalid Session Token: ' + err)
    })
  }

  cleanupFile (req: Request, res: Response, next: NextFunction) {
    let pathName: string = req.filePathName || 'default_path'
    let filename: string = `${pathName}`
    let cleanup = () => {
      fse.remove(`${this.util.FILE_PATH_UPLOADS}/${filename}.zip`)
      fse.remove(`${this.util.FILE_PATH_UNZIPPED}/${filename}`)
    }
    onFinished(res, cleanup)
    next()
  }

  parseFile () {
    let mw = this
    let uploadHandler = multer({
      storage: multer.diskStorage({
        destination (req: Request, file: any, cb: Function) {
          cb(null, mw.util.FILE_PATH_UPLOADS)
        },
        filename (req: Request, file: any, cb: Function) {
          let pathName: string = uuid()
          let filename: string = `${pathName}${path.extname(file.originalname)}`
          req.filePathName = pathName
          cb(null, filename)
        }
      })
    })

    return uploadHandler.single(ZIP_FILE_FIELDNAME)
  }

  forceHTTPS (req: Request, res: Response, next: NextFunction) {
    if (req.headers['x-forwarded-proto'] === 'http') {
      res.writeHead(301, {
        'Location': `https://${req.headers['host']}${req.url}`
      })
      res.end()
      return
    }

    next()
  }
}

export {
  Middleware
}
