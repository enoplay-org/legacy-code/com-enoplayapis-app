import * as express from 'express'

// Add User context to Request
declare interface Request extends express.Request {
  userUID: string
  sessionToken: string
  filePathName: string
}

declare interface Response extends express.Response {}
declare interface NextFunction extends express.NextFunction {}

export {
  Request,
  Response,
  NextFunction
}
