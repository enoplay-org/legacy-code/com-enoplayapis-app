// Import 3rd party packages
import * as express from 'express'
import * as http from 'http'
import * as fse from 'fs-extra'
import * as path from 'path'
import * as cors from 'cors'
import * as bodyParser from 'body-parser'

import {
  MongoOptions,
  MongoHandler,
  HerokuOptions,
  HerokuHandler,
  GitlabOptions,
  GitlabHandler,
  EnoplayStaticOptions,
  EnoplayStaticHandler
} from './infrastructure'

import {
  DbHandler,
  TokenAuthorityOptions,
  DbSessionTokenRepo,
  DbAppRepo,
  DbUserRepo,
  DbGameRepo
} from './interfaces/repository'

import {
  WebHandlerOptions,
  WebHandler
} from './interfaces/web'

import {
  SessionTokenInteractor,
  AppInteractor,
  AppInteractorOptions,
  UserInteractor,
  GameInteractor
} from './usecase'

import {
  APP_REPO_ENOPLAY_GITLAB,
  APP_HOST_HEROKU
} from './domain'

const ENV_PORT = 'PORT'
const ENV_FILE_PATH_UPLOAD_ZIP = 'FILE_PATH_UPLOAD_ZIP'
const ENV_FILE_PATH_UNZIP = 'FILE_PATH_UNZIP'
const ENV_FILE_PATH_CLONE = 'FILE_PATH_CLONE'
const ENV_HOST_URL_WWW = 'HOST_URL_WWW'
const ENV_HOST_URL_MOBILE = 'HOST_URL_MOBILE'
const ENV_HOST_URL_API = 'HOST_URL_API'
const ENV_HOST_URL_STATIC = 'HOST_URL_STATIC'
const ENV_HOST_URL_CDN = 'HOST_URL_CDN'
// Signing keys
const ENV_PRIVATE_SIGNING_KEY = 'PRIVATE_SIGNING_KEY'
const ENV_PUBLIC_SIGNING_KEY = 'PUBLIC_SIGNING_KEY'
// Mongo
const ENV_MONGO_DATABASE = 'MONGO_DATABASE'
const ENV_MONGO_FULLURI = 'MONGO_FULLURI'
// Gitlab
const ENV_ENOPLAY_GITLAB_HOST_URL_API = 'ENOPLAY_GITLAB_HOST_URL_API'
const ENV_ENOPLAY_GITLAB_HOST_DOMAIN = 'ENOPLAY_GITLAB_HOST_DOMAIN'
const ENV_ENOPLAY_GITLAB_TOKEN = 'ENOPLAY_GITLAB_TOKEN'
const ENV_ENOPLAY_GITLAB_AUTHOR_USERNAME = 'ENOPLAY_GITLAB_AUTHOR_USERNAME'
const ENV_ENOPLAY_GITLAB_AUTHOR_PASS = 'ENOPLAY_GITLAB_AUTHOR_PASS'
const ENV_ENOPLAY_GITLAB_AUTHOR_NAME = 'ENOPLAY_GITLAB_AUTHOR_NAME'
const ENV_ENOPLAY_GITLAB_AUTHOR_EMAIL = 'ENOPLAY_GITLAB_AUTHOR_EMAIL'
const ENV_ENOPLAY_GITLAB_NAMESPACE_ID = 'ENOPLAY_GITLAB_NAMESPACE_ID'
const ENV_ENOPLAY_GITLAB_NAMESPACE_NAME = 'ENOPLAY_GITLAB_NAMESPACE_NAME'
const ENV_ENOPLAY_GITLAB_NAMESPACE_DESCRIPTION = 'ENOPLAY_GITLAB_NAMESPACE_DESCRIPTION'
const ENV_ENOPLAY_GITLAB_REMOTE_NAME = 'ENOPLAY_GITLAB_REMOTE_NAME'
// Heroku
const ENV_HEROKU_TOKEN = 'HEROKU_TOKEN'

const configPath = path.resolve(__dirname, '../')

// Setup environment variables
initEnvironmentVariables()

// Setup MongoDB - Database service
let mongoOptions = new MongoOptions(process.env[ENV_MONGO_FULLURI], process.env[ENV_MONGO_DATABASE])
let mongoHandler = new MongoHandler(mongoOptions)
mongoHandler.newSession()

let dbHandlers: { [key: string]: DbHandler } = {}
dbHandlers[DbSessionTokenRepo.getClassName()] = mongoHandler
dbHandlers[DbAppRepo.getClassName()] = mongoHandler
dbHandlers[DbUserRepo.getClassName()] = mongoHandler
dbHandlers[DbGameRepo.getClassName()] = mongoHandler

// Setup Enoplay Gitlab - Git repository service
let enoplayGitlabServiceName = 'Enoplay Gitlab'

let enoplayGitlabOptions = new GitlabOptions(
  process.env[ENV_ENOPLAY_GITLAB_HOST_URL_API],
  process.env[ENV_ENOPLAY_GITLAB_HOST_DOMAIN],
  process.env[ENV_ENOPLAY_GITLAB_TOKEN],
  APP_REPO_ENOPLAY_GITLAB,
  enoplayGitlabServiceName, {
    username: process.env[ENV_ENOPLAY_GITLAB_AUTHOR_USERNAME],
    pass: process.env[ENV_ENOPLAY_GITLAB_AUTHOR_PASS],
    name: process.env[ENV_ENOPLAY_GITLAB_AUTHOR_NAME],
    email: process.env[ENV_ENOPLAY_GITLAB_AUTHOR_EMAIL]
  }, {
    ID: process.env[ENV_ENOPLAY_GITLAB_NAMESPACE_ID],
    name: process.env[ENV_ENOPLAY_GITLAB_NAMESPACE_NAME],
    description: process.env[ENV_ENOPLAY_GITLAB_NAMESPACE_DESCRIPTION]
  }, process.env[ENV_ENOPLAY_GITLAB_REMOTE_NAME])
let enoplayGitlabHandler = new GitlabHandler(enoplayGitlabOptions, process.env[ENV_FILE_PATH_CLONE])

// Setup Heroku - Application hosting service
let herokuOptions = new HerokuOptions(process.env[ENV_HEROKU_TOKEN], APP_HOST_HEROKU)
let herokuHandler = new HerokuHandler(herokuOptions)

// Setup EnoplayStatic - Image management service
let enoplayStaticOptions = new EnoplayStaticOptions(process.env[ENV_HOST_URL_STATIC])
let enoplayStaticHandler = new EnoplayStaticHandler(enoplayStaticOptions)

// Setup interactors & repositories
let sessionTokenInteractor = new SessionTokenInteractor()
let appInteractor = new AppInteractor(new AppInteractorOptions(
  process.env[ENV_HOST_URL_WWW],
  process.env[ENV_HOST_URL_MOBILE],
  process.env[ENV_HOST_URL_API],
  process.env[ENV_HOST_URL_STATIC],
  process.env[ENV_HOST_URL_CDN]
))
let userInteractor = new UserInteractor()
let gameInteractor = new GameInteractor()

let tokenAuthorityOptions = new TokenAuthorityOptions(process.env[ENV_PRIVATE_SIGNING_KEY], process.env[ENV_PUBLIC_SIGNING_KEY])
sessionTokenInteractor.setSessionTokenRepository(new DbSessionTokenRepo(dbHandlers, tokenAuthorityOptions))
appInteractor.setAppRepository(new DbAppRepo(dbHandlers, enoplayGitlabHandler, herokuHandler))
userInteractor.setUserRepository(new DbUserRepo(dbHandlers))
gameInteractor.setGameRepository(new DbGameRepo(dbHandlers, enoplayStaticHandler))

gameInteractor.setAppRepository(appInteractor.appRepository)
gameInteractor.setUserRepository(userInteractor.userRepository)

appInteractor.setGameRepository(gameInteractor.gameRepository)
appInteractor.setUserRepository(userInteractor.userRepository)

// Setup web handler
let webHandlerOptions = new WebHandlerOptions(process.env[ENV_FILE_PATH_UPLOAD_ZIP], process.env[ENV_FILE_PATH_UNZIP])
let w = new WebHandler(webHandlerOptions)
w.setSessionTokenInteractor(sessionTokenInteractor)
w.setAppInteractor(appInteractor)
w.setUserInteractor(userInteractor)
w.setGameInteractor(gameInteractor)

// Setup route handling
let router = express()
router.use(w.mw.forceHTTPS.bind(w.mw))
router.use(cors())
router.use(bodyParser.json())
router.get('/', w.meta.getIndex.bind(w.meta))

// Web API (Version 0)
let v0 = express()
v0.get('/apps/:aid',
  w.mw.ensureAuthorization.bind(w.mw),
  w.app.retrieve.bind(w.app)) // get app
v0.get('/apps/:aid/status',
  w.mw.ensureAuthorization.bind(w.mw),
  w.app.getStatus.bind(w.app)) // get app status (e.g. build, runtime)
v0.get('/apps/:aid/versions',
  w.mw.ensureAuthorization.bind(w.mw),
  w.app.getVersions.bind(w.app)) // get app versions
v0.post('/apps',
  bodyParser.urlencoded({ extended: true }),
  w.mw.ensureAuthorization.bind(w.mw),
  w.mw.parseFile.call(w.mw),
  w.mw.cleanupFile.bind(w.mw),
  w.app.create.bind(w.app)) // create an app
v0.post('/apps/:aid/file',
  bodyParser.urlencoded({ extended: true }),
  w.mw.ensureAuthorization.bind(w.mw),
  w.mw.parseFile.call(w.mw),
  w.mw.cleanupFile.bind(w.mw),
  w.app.updateFile.bind(w.app)) // update app file
v0.post('/apps/:aid/version_prod',
  w.mw.ensureAuthorization.bind(w.mw),
  w.app.updateVersionProd.bind(w.app)) // rollback to version
v0.delete('/apps/:aid',
  w.mw.ensureAuthorization.bind(w.mw),
  w.app.delete.bind(w.app)) // delete app

router.use('/v0', v0)
const server = http.createServer(router)
server.listen(process.env[ENV_PORT])
server.on('error', onServerError)
server.on('listening', onServerSuccess)

function onServerError (err: any) {
  console.log(`Error running server: ${err}`)
}

function onServerSuccess () {
  console.log(`Listening on port: ${process.env[ENV_PORT]}`)
}

function initEnvironmentVariables () {
  // Ensure index.json exists
  let indexFileExists = fse.existsSync(path.resolve(configPath, 'config/index.json'))
  if (!indexFileExists) {
    return
  }

  // Setup general settings
  let indexFile = fse.readFileSync(path.resolve(configPath, 'config/index.json'))
  let indexConfig = JSON.parse(indexFile.toString())

  // Setup PORT
  if (!process.env[ENV_PORT]) {
    process.env[ENV_PORT] = indexConfig.port
  }

  // Setup filePathUpload
  if (!process.env[ENV_FILE_PATH_UPLOAD_ZIP]) {
    process.env[ENV_FILE_PATH_UPLOAD_ZIP] = indexConfig.filePathUploadZip
  }
  process.env[ENV_FILE_PATH_UPLOAD_ZIP] = path.resolve(process.env[ENV_FILE_PATH_UPLOAD_ZIP])

  // Setup filePathUnzip
  if (!process.env[ENV_FILE_PATH_UNZIP]) {
    process.env[ENV_FILE_PATH_UNZIP] = indexConfig.filePathUnzip
  }
  process.env[ENV_FILE_PATH_UNZIP] = path.resolve(process.env[ENV_FILE_PATH_UNZIP])

  // Setup filePathClone
  if (!process.env[ENV_FILE_PATH_CLONE]) {
    process.env[ENV_FILE_PATH_CLONE] = indexConfig.filePathClone
  }
  process.env[ENV_FILE_PATH_CLONE] = path.resolve(process.env[ENV_FILE_PATH_CLONE])

  // Setup Host URL - www
  if (!process.env[ENV_HOST_URL_WWW]) {
    process.env[ENV_HOST_URL_WWW] = indexConfig.wwwwHostURL
  }

  // Setup Host URL - mobile
  if (!process.env[ENV_HOST_URL_MOBILE]) {
    process.env[ENV_HOST_URL_MOBILE] = indexConfig.mobileHostURL
  }

  // Setup Host URL - api
  if (!process.env[ENV_HOST_URL_API]) {
    process.env[ENV_HOST_URL_API] = indexConfig.apiHostURL
  }

  // Setup Host URL - static
  if (!process.env[ENV_HOST_URL_STATIC]) {
    process.env[ENV_HOST_URL_STATIC] = indexConfig.staticHostURL
  }

  // Setup Host URL - cdn
  if (!process.env[ENV_HOST_URL_CDN]) {
    process.env[ENV_HOST_URL_CDN] = indexConfig.cdnHostURL
  }

  // Setup private signing key
  if (!process.env[ENV_PRIVATE_SIGNING_KEY]) {
    let privateKeyBuffer = fse.readFileSync(path.resolve(configPath, 'config/keys/api.domain.key'))
    process.env[ENV_PRIVATE_SIGNING_KEY] = privateKeyBuffer.toString()
  }

  // Setup public signing key
  if (!process.env[ENV_PUBLIC_SIGNING_KEY]) {
    let publicKeyBuffer = fse.readFileSync(path.resolve(configPath, 'config/keys/api.domain.key.pub'))
    process.env[ENV_PUBLIC_SIGNING_KEY] = publicKeyBuffer.toString()
  }

  // Setup Mongo (Database and FullURI)
  let mongoFile = fse.readFileSync(path.resolve(configPath, 'config/mongo.json'))
  let mongoConfig = JSON.parse(mongoFile.toString())

  // Mongo Database
  if (!process.env[ENV_MONGO_DATABASE]) {
    process.env[ENV_MONGO_DATABASE] = mongoConfig.database
  }

  // Mongo FullURI
  if (!process.env[ENV_MONGO_FULLURI]) {
    process.env[ENV_MONGO_FULLURI] = mongoConfig.fullURI
  }

  // Setup Enoplay Gitlab (token, author, namespace and remoteName)
  let enoplayGitlabFile = fse.readFileSync(path.resolve(configPath, 'config/enoplay-gitlab.json'))
  let enoplayGitlabConfig = JSON.parse(enoplayGitlabFile.toString())

  // Enoplay Gitlab URL
  if (!process.env[ENV_ENOPLAY_GITLAB_HOST_URL_API]) {
    process.env[ENV_ENOPLAY_GITLAB_HOST_URL_API] = enoplayGitlabConfig.apiURL
  }

  // Enoplay Gitlab URL
  if (!process.env[ENV_ENOPLAY_GITLAB_HOST_DOMAIN]) {
    process.env[ENV_ENOPLAY_GITLAB_HOST_DOMAIN] = enoplayGitlabConfig.domain
  }

  // Enoplay Gitlab token
  if (!process.env[ENV_ENOPLAY_GITLAB_TOKEN]) {
    process.env[ENV_ENOPLAY_GITLAB_TOKEN] = enoplayGitlabConfig.token
  }

  // Enoplay Gitlab author
  if (!process.env[ENV_ENOPLAY_GITLAB_AUTHOR_USERNAME]) {
    process.env[ENV_ENOPLAY_GITLAB_AUTHOR_USERNAME] = enoplayGitlabConfig.author.username
    process.env[ENV_ENOPLAY_GITLAB_AUTHOR_PASS] = enoplayGitlabConfig.author.pass
    process.env[ENV_ENOPLAY_GITLAB_AUTHOR_EMAIL] = enoplayGitlabConfig.author.email
    process.env[ENV_ENOPLAY_GITLAB_AUTHOR_NAME] = enoplayGitlabConfig.author.name
  }

  // Enoplay Gitlab namespace
  if (!process.env[ENV_ENOPLAY_GITLAB_NAMESPACE_ID]) {
    process.env[ENV_ENOPLAY_GITLAB_NAMESPACE_ID] = enoplayGitlabConfig.namespace.ID
    process.env[ENV_ENOPLAY_GITLAB_NAMESPACE_NAME] = enoplayGitlabConfig.namespace.name
    process.env[ENV_ENOPLAY_GITLAB_NAMESPACE_DESCRIPTION] = enoplayGitlabConfig.namespace.description
  }

  // Enoplay Gitlab remoteName
  if (!process.env[ENV_ENOPLAY_GITLAB_REMOTE_NAME]) {
    process.env[ENV_ENOPLAY_GITLAB_REMOTE_NAME] = enoplayGitlabConfig.remoteName
  }

  // Setup Heroku (token)
  let herokuFile = fse.readFileSync(path.resolve(configPath, 'config/heroku.json'))
  let herokuConfig = JSON.parse(herokuFile.toString())

  // Heroku token
  if (!process.env[ENV_HEROKU_TOKEN]) {
    process.env[ENV_HEROKU_TOKEN] = herokuConfig.token
  }
}
