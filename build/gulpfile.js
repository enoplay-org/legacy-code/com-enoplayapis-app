var gulp = require('gulp')
var path = require('path')
var mocha = require('gulp-mocha')
var nodemon = require('gulp-nodemon')
var clean = require('gulp-clean')
var ts = require('gulp-typescript')
var gulpTslint = require('gulp-tslint')
var tslint = require('tslint')
var runSequence = require('run-sequence')
var replace = require('gulp-replace')
var merge = require('merge-stream')

var envConfig = require('./env')

var srcPath = envConfig.srcPath
var distPath = envConfig.distPath
var unitSrcPath = envConfig.unitSrcPath
var unitDistPath = envConfig.unitDistPath

// run mocha tests in the ./test folder
gulp.task('mocha', function () {
  return gulp.src(`${unitDistPath}/**/*.js`, { read: false })
      .pipe(mocha())
})

gulp.task('test_unit_typescript', function () {
  var tsProject = ts.createProject(`${unitSrcPath}/tsconfig.json`)
  return gulp.src([`${unitSrcPath}/**/*.ts`])
    .pipe(tsProject())
    .pipe(gulp.dest(`${unitDistPath}`))
})

gulp.task('test_unit_clean', function () {
  return gulp.src(`${unitDistPath}`, { read: false })
    .pipe(clean({ force: true }))
})

gulp.task('test_unit_files_copy', function () {
  var interface_web = gulp.src([`${unitSrcPath}/interfaces/web/files_test/**/*`])
    .pipe(gulp.dest(`${unitDistPath}/interfaces/web/files_test`))

  var usecase = gulp.src([`${unitSrcPath}/usecase/files_test/**/*`])
    .pipe(gulp.dest(`${unitDistPath}/usecase/files_test`))

  return merge(interface_web, usecase)
})

gulp.task('test_unit_paths_replace', function(){
  gulp.src([`${unitDistPath}/**/*.js`])
    .pipe(replace('src', 'dist'))
    .pipe(gulp.dest(`${unitDistPath}`))
})

// run nodemon on server file changes
gulp.task('nodemon', function (cb) {
  var started = false;
  return nodemon({
    script: `${distPath}/main.js`,
    watch: [`${distPath}/*.js`]
  }).on('start', function () {
    if (!started) {
        cb();
        started = true;
    }
  })
})

gulp.task('watch', function () {
    gulp.watch(`${srcPath}/**/*.ts`, ['build'])
})

gulp.task('clean', function () {
  return gulp.src(`${distPath}`, { read: false })
    .pipe(clean({ force: true }))
})

gulp.task('typescript', function () {
  var tsProject = ts.createProject(`${srcPath}/tsconfig.json`)
  return gulp.src([`${srcPath}/**/*.ts`])
    .pipe(tsProject())
    .pipe(gulp.dest(`${distPath}`))
})

gulp.task('tslint', function () {
  var app_files = `${srcPath}/usecase/app_files/**`

  var program = tslint.Linter.createProgram(`${srcPath}/tsconfig.json`)

  return gulp.src([`${srcPath}/**/*.ts`, `!${app_files}`], { base: '.' })
    .pipe(gulpTslint({
      program,
      formatter: "verbose"
    }))
    .pipe(gulpTslint.report())
})

gulp.task('app_files_copy', function () {
  return gulp.src([`${srcPath}/usecase/app_files/**/*`])
    .pipe(gulp.dest(`${distPath}/usecase/app_files`))
})

gulp.task('build', function (done) {
  runSequence(
    'clean',
    'tslint',
    'typescript',
    'app_files_copy',
  function () {
    done()
  })
})

gulp.task('unit', function (done) {
  runSequence(
    'test_unit_clean',
    'test_unit_typescript',
    'test_unit_paths_replace',
    'test_unit_files_copy',
    'build',
    'mocha',
  function () {
    done()
  })
})

gulp.task('serve', ['nodemon'])
