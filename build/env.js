var path = require('path')

module.exports = {
  srcPath: path.resolve(__dirname, '../src'),
  distPath: path.resolve(__dirname, '../dist'),
  unitSrcPath: path.resolve(__dirname, '../test/unit'),
  unitDistPath: path.resolve(__dirname, '../test/unit_dist')
}
