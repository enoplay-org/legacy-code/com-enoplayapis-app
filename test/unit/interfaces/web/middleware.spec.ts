import * as assert from 'assert'
import * as mocha from 'mocha'

import * as path from 'path'
import * as http from 'http'
import * as express from 'express'
import * as fse from 'fs-extra'
import * as request from 'request'
import * as bodyParser from 'body-parser'

import { Request, Response, NextFunction } from '../../../../src/interfaces/web/express.d'
import { WebHandler, WebHandlerOptions } from '../../../../src/interfaces/web'
import { ZIP_FILE_FIELDNAME } from '../../../../src/interfaces/web/util'
import { TEST_PORT, TEST_ADDRESS, FILE_PATH_UPLOADS, FILE_PATH_UNZIPPED } from './config'

let w: WebHandler
let router: express.Express

// Setup web handler
let webHandlerOptions = new WebHandlerOptions(FILE_PATH_UPLOADS, FILE_PATH_UNZIPPED)
w = new WebHandler(webHandlerOptions)
router = express()
router.use(bodyParser.json())
router.post('/test',
  bodyParser.urlencoded({ extended: true }),
  w.mw.parseFile.call(w.mw),
  w.mw.cleanupFile.bind(w.mw),
  testHandler)
const server = http.createServer(router)
server.listen(TEST_PORT, TEST_ADDRESS)
server.on('error', onServerError)
server.on('listening', onServerSuccess)

function onServerError (err: any) {
  console.log(`Error running server: ${err}`)
}
function onServerSuccess () {
  console.log(`Listening on ${server.address().address}:${server.address().port}`)
}

describe('Web Middleware', function () {
  this.timeout(15000)
  let zipFilePath = path.resolve(__dirname, 'files_test/spa_app.zip')
  let filePathName = ''

  beforeEach(function (done: MochaDone) {
    let formData = {
      [ZIP_FILE_FIELDNAME]: fse.createReadStream(zipFilePath)
    }

    request.post({
      url: `http://${TEST_ADDRESS}:${TEST_PORT}/test`,
      formData: formData
    }, function (err: any, response: request.RequestResponse, body: any) {
      if (err) {
        return console.error('Uplaod failed: ', err)
      }
      body = JSON.parse(body)
      filePathName = body.filePathName
      done()
    })
  })

  describe('#cleanupFile', function () {
    const WAIT_TIME = 1000 // wait 1 second after request for files to be deleted

    it(`should remove zip file from /uploads`, function (done: MochaDone) {
      console.log('Waiting for async cleanup to complete...')
      setTimeout(function () {
        fse.pathExists(`${FILE_PATH_UPLOADS}/${filePathName}.zip`)
        .then(exists => {
          assert.equal(false, exists)
          done()
        })
        .catch(done)
      }, WAIT_TIME)
    })
    it(`should remove unzipped file from /unzipped`, function (done: MochaDone) {
      console.log('Waiting for async cleanup to complete...')
      setTimeout(function () {
        fse.pathExists(`${FILE_PATH_UNZIPPED}/${filePathName}`)
        .then(exists => {
          assert.equal(false, exists)
          done()
        })
        .catch(done)
      }, WAIT_TIME)
    })
  })
})

function testHandler (req: Request, res: Response) {
  // Unzip the file
  let fileName = req.file.filename.slice(0, -4)
  return w.mw.util.unzipFile.call(w.mw.util, fileName)
  .then(() => {
    res.json({
      filePathName: fileName,
      message: 'Test finished',
      success: (req.file ? true : false)
    })
  })
  .catch((err: any) => {
    res.json({ message: `Test Failed to unzip file: ${err}`, success: false })
  })
}
