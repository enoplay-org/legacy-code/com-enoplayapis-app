import * as path from 'path'

const FILE_PATH_APP: string = path.resolve(__dirname, './files_test')

export {
  FILE_PATH_APP
}
