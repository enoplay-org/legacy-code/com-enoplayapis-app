import * as assert from 'assert'
import * as mocha from 'mocha'

import * as fse from 'fs-extra'

import {
  AppDetails,
  ensureIndexHTML,
  updateIndexHTML,
  AppInteractorOptions
} from '../../../src/usecase/manageApp'
import { Game, User } from '../../../src/domain'
import { FILE_PATH_APP } from './config'

let originalIndexHTML: string = ''

describe('Manage App', function () {
  beforeEach(function (done: MochaDone) {
    // Setup original index.html
    fse.readFile(`${FILE_PATH_APP}/index-original.html`)
    .then(buffer => {
      originalIndexHTML = buffer.toString()
      done()
    })
    .catch(done)
  })

  describe('#ensureIndexHTML', function () {
    let doesExist: boolean = false

    beforeEach(function (done: MochaDone) {
      ensureIndexHTML(FILE_PATH_APP)
      .then(() => {
        doesExist = true
      })
      .catch(() => {
        doesExist = false
      })
      .then(done)
    })

    it('should not throw an error when app/index.html exists', function () {
      assert.equal(doesExist, true)
    })
  })

  describe('#updateIndexHTML', function () {
    beforeEach(function (done: MochaDone) {
      let details: AppDetails = {
        title: '',
        language: '',
        game: new Game(),
        publisher: new User(),
        options: new AppInteractorOptions('www', 'mobile', 'api', 'static', 'cdn')
      }
      updateIndexHTML(FILE_PATH_APP, details)
      .then(() => {
        done()
      })
      .catch(done)
    })

    it('should add auth.js script to index.html', function (done: MochaDone) {
      fse.readFile(`${FILE_PATH_APP}/index.html`)
      .then(buffer => {
        let indexHTML = buffer.toString()

        let hasAuthScript: boolean = indexHTML.indexOf('/js/client/game/auth.js') >= 0
        assert.notEqual(indexHTML, originalIndexHTML)
        assert.equal(hasAuthScript, true)
        done()
      })
      .catch(done)
    })
  })
})
