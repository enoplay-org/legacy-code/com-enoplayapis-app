"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const path = require("path");
const http = require("http");
const express = require("express");
const fse = require("fs-extra");
const request = require("request");
const bodyParser = require("body-parser");
const web_1 = require("../../../../dist/interfaces/web");
const util_1 = require("../../../../dist/interfaces/web/util");
const config_1 = require("./config");
let w;
let router;
let webHandlerOptions = new web_1.WebHandlerOptions(config_1.FILE_PATH_UPLOADS, config_1.FILE_PATH_UNZIPPED);
w = new web_1.WebHandler(webHandlerOptions);
router = express();
router.use(bodyParser.json());
router.post('/test', bodyParser.urlencoded({ extended: true }), w.mw.parseFile.call(w.mw), w.mw.cleanupFile.bind(w.mw), testHandler);
const server = http.createServer(router);
server.listen(config_1.TEST_PORT, config_1.TEST_ADDRESS);
server.on('error', onServerError);
server.on('listening', onServerSuccess);
function onServerError(err) {
    console.log(`Error running server: ${err}`);
}
function onServerSuccess() {
    console.log(`Listening on ${server.address().address}:${server.address().port}`);
}
describe('Web Middleware', function () {
    this.timeout(15000);
    let zipFilePath = path.resolve(__dirname, 'files_test/spa_app.zip');
    let filePathName = '';
    beforeEach(function (done) {
        let formData = {
            [util_1.ZIP_FILE_FIELDNAME]: fse.createReadStream(zipFilePath)
        };
        request.post({
            url: `http://${config_1.TEST_ADDRESS}:${config_1.TEST_PORT}/test`,
            formData: formData
        }, function (err, response, body) {
            if (err) {
                return console.error('Uplaod failed: ', err);
            }
            body = JSON.parse(body);
            filePathName = body.filePathName;
            done();
        });
    });
    describe('#cleanupFile()', function () {
        const WAIT_TIME = 1000;
        it(`should remove zip file from /uploads`, function (done) {
            console.log('Waiting for async cleanup to complete...');
            setTimeout(function () {
                fse.pathExists(`${config_1.FILE_PATH_UPLOADS}/${filePathName}.zip`)
                    .then(exists => {
                    assert.equal(false, exists);
                    done();
                })
                    .catch(done);
            }, WAIT_TIME);
        });
        it(`should remove unzipped file from /unzipped`, function (done) {
            console.log('Waiting for async cleanup to complete...');
            setTimeout(function () {
                fse.pathExists(`${config_1.FILE_PATH_UNZIPPED}/${filePathName}`)
                    .then(exists => {
                    assert.equal(false, exists);
                    done();
                })
                    .catch(done);
            }, WAIT_TIME);
        });
    });
});
function testHandler(req, res) {
    let fileName = req.file.filename.slice(0, -4);
    return w.mw.util.unzipFile.call(w.mw.util, fileName)
        .then(() => {
        res.json({
            filePathName: fileName,
            message: 'Test finished',
            success: (req.file ? true : false)
        });
    })
        .catch((err) => {
        res.json({ message: `Test Failed to unzip file: ${err}`, success: false });
    });
}
