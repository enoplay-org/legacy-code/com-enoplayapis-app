"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const fse = require("fs-extra");
const manageApp_1 = require("../../../dist/usecase/manageApp");
const domain_1 = require("../../../dist/domain");
const config_1 = require("./config");
let originalIndexHTML = '';
describe('Manage App', function () {
    beforeEach(function (done) {
        fse.readFile(`${config_1.FILE_PATH_APP}/index-original.html`)
            .then(buffer => {
            originalIndexHTML = buffer.toString();
            done();
        })
            .catch(done);
    });
    describe('#ensureIndexHTML', function () {
        let doesExist = false;
        beforeEach(function (done) {
            manageApp_1.ensureIndexHTML(config_1.FILE_PATH_APP)
                .then(() => {
                doesExist = true;
            })
                .catch(() => {
                doesExist = false;
            })
                .then(done);
        });
        it('should not throw an error when app/index.html exists', function () {
            assert.equal(doesExist, true);
        });
    });
    describe('#updateIndexHTML', function () {
        beforeEach(function (done) {
            let details = {
                title: '',
                language: '',
                game: new domain_1.Game(),
                publisher: new domain_1.User(),
                options: new manageApp_1.AppInteractorOptions('www', 'mobile', 'api', 'static', 'cdn')
            };
            manageApp_1.updateIndexHTML(config_1.FILE_PATH_APP, details)
                .then(() => {
                done();
            })
                .catch(done);
        });
        it('should add auth.js script to index.html', function (done) {
            fse.readFile(`${config_1.FILE_PATH_APP}/index.html`)
                .then(buffer => {
                let indexHTML = buffer.toString();
                let hasAuthScript = indexHTML.indexOf('/js/client/game/auth.js') >= 0;
                assert.notEqual(indexHTML, originalIndexHTML);
                assert.equal(hasAuthScript, true);
                done();
            })
                .catch(done);
        });
    });
});
